#include <iostream>

int 
main()
{
    int counter = 0;
    double number;
    std::cout << "\nInput number " << counter <<": ";
    std::cin >> number;
    double largest1 = number, largest2 = largest1;
    ++counter;
    while (counter < 10) {
        std::cout << "\nInput number " << counter <<": ";
        std::cin >> number;
        if (number > largest1) {
            largest2 = largest1;
            largest1 = number;
        } else if (number > largest2) {
            largest2 = number;
        }
        ++counter;
    }
    std::cout << "\nThe largest1 number is: " << largest1 << std::endl;
    std::cout << "\nThe largest2 number is: " << largest2 << std::endl;
    return 0;
}
