#include <iostream>

int
main()
{
    int row = 1, column = 1, length = 16, width = 8;
    while (row <= width) {
        while (column <= length) {
            if ((row % 2) == 1) {
                if ((column % 2) == 1) {
                    std::cout << "*";
                } else {
                    std::cout << " ";
                }
            } else {
                if ((column % 2) == 0) {
                    std::cout << "*";
                } else {
                    std::cout << " ";
                }
            }
            ++column;
        }
        column = 1;
        ++row;
        std::cout << std::endl;
    }
    std::cout << std::endl;
    return 0;
}
