#include <iostream>

int
main()
{
    int a, b;
    std::cout << "Enter two numbers: ";
    std::cin >> a >> b;
    if(b != 0){
        if(a % b == 0){
            std::cout << "Yes" << std::endl;
        }
        if(a % b != 0){
            std::cout << "No" << std::endl;
        }
        
        return 0;
    }

    std::cout << "Error\n";
    return 1; ///Error return1 
}
