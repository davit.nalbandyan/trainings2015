 /// Inputs three integers from keyboard and
 /// prints the sum, average, product,
 /// smallest and largest of thesse numbers.

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    ///data initialization
    int number1 = 0, number2 = 0, number3 = 0;
    
    /// screen dilog and input data
    std::cout << "Input three different integers: ";
    std::cin >> number1 >> number2 >> number3;

    /// find sum, average, product
    int sum = number1 + number2 + number3;
    int average = sum / 3;
    int product = number1 * number2 * number3;

    /// find min and max
    int min = number1, max = number1;

    if(min > number2) { /// if min greater then number2
	min = number2; /// min assign to number2
    } else if(number2 > max) { /// else if number2 greater then max 
        max = number2; /// max assign to number2
    }
    if(min > number3) { /// if min greater then number
        min = number3; /// min assign to number3
    } else if(number3 > max) { /// else if number3 greater then max
        max = number3; /// max assign to number3
    }
    
    
    /// output result
    std::cout << "Sum is " << sum << std::endl;
    std::cout << "Average is " << average << std::endl;
    std::cout << "Product is " << product << std::endl;
    std::cout << "Smallest is " << min << std::endl;
    std::cout << "Largest is " << max << std::endl;

    return 0; /// program ends successfully
} /// end function main
