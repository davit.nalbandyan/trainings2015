/// Program prints a box, an oval, an arrow and a dimond

#include <iostream> /// allows program to use input and output

/// function main begins program execution
int
main()
{
    std::cout << " *********       ***        *         *" << std::endl;
    std::cout << " *       *     *     *     ***       * *" << std::endl;
    std::cout << " *       *    *       *   *****     *   *" << std::endl;
    std::cout << " *       *    *       *     *      *     *" << std::endl;
    std::cout << " *       *    *       *     *     *       *" << std::endl;
    std::cout << " *       *    *       *     *      *     *" << std::endl;
    std::cout << " *       *    *       *     *       *   *" << std::endl;
    std::cout << " *       *     *     *      *        * *" << std::endl;
    std::cout << " *********       ***        *         *" << std::endl;

    return 0; /// program ends successfully
} /// end function main
