/// Prints the numbers 1 2 3 4 in one line 

#include <iostream> /// allows program to perform intput and output

/// function main begins program execution
int
main()
{
    /// outputing data in different ways.
    std::cout << "1 2 3 4" << std::endl; /// output four numbers together in one string
    std::cout << "1 " << "2 " << "3 " << "4 " << std::endl; /// output four numbers
    std::cout << "1 "; /// output one number
    std::cout << "2 "; /// output one number
    std::cout << "3 "; /// output one number
    std::cout << "4 " << std::endl; /// output one number and endline

    return 0; /// program ended successfully
}/// end function main
