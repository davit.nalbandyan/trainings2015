/// The program inputs a five digit integer,
/// separates integer into its digits and
/// prints them separated by three spaces each.

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program execution
int
main()
{
    /// initialization of data
    int number = 0;

    /// input data
    std::cout << "Enter a five digit integer (10000 - 99999) : ";
    std::cin >> number;

    /// error handleing
    if(number < 10000) { /// if number out of range
        std::cout << "Invalid number!" << std::endl; /// print error message
        return 1; /// program ends with error
    } /// endif
    if(number > 99999) { /// if number out of range
        std::cout << "Invalid number!" << std::endl; /// print error message
        return 1; /// program ends with error
    } /// endif

    /// print digits seperated by three spaces
    std::cout << number / 10000 % 10 << std::setw(4)
              << number / 1000 % 10 << std::setw(4)
              << number / 100 % 10 << std::setw(4)
              << number / 10 % 10 << std::setw(4)
              << number / 1 % 10 << std::endl;
    
    return 0; /// program ends successfully
} /// end function main
