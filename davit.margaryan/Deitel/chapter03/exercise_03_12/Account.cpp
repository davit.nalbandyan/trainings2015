/// Account.cpp
/// The implimentetion of the Account class definition member-functions

#include "Account.hpp" /// includes definition of the Account class
#include <iostream> /// allows program to perform input and output

/// constructor validates and initializw balance_
Account::Account(int balance)
{
    if(balance < 0) { /// if balance is invalid
        balance_ = 0;
        std::cout << "Invalid initial balance! Reset to 0." /// error message
                  << std::endl; 
    } /// end if
    if(balance >= 0) { /// if balance is valid
        balance_ = balance;
    } /// endif
} /// end constructor

/// function debit withdrow money from balance
void
Account::debit(int money)
{
    if(money > balance_) { /// if money greather then balance
        std::cout << "Debit amount exceeded account balance." << std::endl;
    } /// end if
    if(money <= balance_) { /// if money smoller then balance
        balance_ = balance_ - money; /// withdrow money
    } /// end if
} 

/// function credig add amount to balance
void
Account::credit(int amount)
{
    balance_ = balance_ + amount; /// add amount
}

/// get balance
int
Account::getBalance()
{
    return balance_;
}
