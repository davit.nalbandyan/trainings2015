  Usually fatal error leads the immediate termination of the program. 
The program can not do its job  and we can easily correct the mistake.
  Non-fatal errors allows to continuation of the program, but 
the latter often produces incorrect results. 
That is why a fatal error may be more desirable.
