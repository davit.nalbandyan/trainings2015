#include <iostream>

int
main()
{
    for (int leg1 = 1; leg1 <= 500; ++leg1){
        int leg1Sq = leg1 * leg1;         
        for (int leg2 = leg1; leg2 <= 500; ++leg2){
            int leg2Sq = leg2 * leg2;    
            for (int hypotenuse = leg2 + 1; hypotenuse < leg1 + leg2 && hypotenuse <= 500; ++hypotenuse){
                int hypotenuseSq = hypotenuse * hypotenuse;
                if (leg1Sq + leg2Sq == hypotenuseSq){
                    std::cout << "leg1: " << leg1 << "\tleg2: " << leg2 
                        << "\thypotenuse: " << hypotenuse << std::endl;
                    break;    
                }
            }
        }
    }

    return 0;
}
