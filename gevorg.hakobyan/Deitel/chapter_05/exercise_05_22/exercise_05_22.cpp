#include <iostream>

int
main()
{
    int x = 9, y = 2;

    if (!(x < 5) && !(y >= 7) && !((x < 5) || (y >= 7))){
        std::cout << "!(x < 5) && !(y >= 7) and !((x < 5) || (y >= 7)) are equivalent." << std::endl;
    } else {
        std::cout << "!(x < 5) && !(y >= 7) and !((x < 5) || (y >= 7)) are not equivalent." << std::endl;
    }

    int a = 2, g = 5, b = 3;

    if (!(a == b) || !(g != 5) && !((a == b) && (g != 5))){
        std::cout << "!(a == b) || !(g != 5) and !((a == b) && (g != 5))) are equivalent." << std::endl;
    } else {
        std::cout << "!(a == b) || !(g != 5) and !((a == b) && (g != 5))) are not equivalent." << std::endl;
    }

    if (!((x <= 8) && (y > 4)) && !(x <= 8) || !(y > 4)){
        std::cout << "!((x <= 8) && (y > 4)) and !(x <= 8) || !(y > 4) are equivalent." << std::endl;
    } else {
        std::cout << "!((x <= 8) && (y > 4)) and !(x <= 8) || !(y > 4) are not equivalent." << std::endl;
    }

    int i = 4, j = 7;

    if (!((i > 4) || (j <= 6)) && !(i > 4) && !(j <= 6)){
        std::cout << "!((i > 4) || (j <= 6)) and !(i > 4) && !(j <= 6) are equivalent." << std::endl;
    } else {
        std::cout << "!((i > 4) || (j <= 6)) and !(i > 4) && !(j <= 6) are not equivalent." << std::endl;
    }

    return 0;
}
