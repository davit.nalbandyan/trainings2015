#include <iostream>
#include <cmath>
#include <iomanip>

int
main()
{
    for (int rate = 5; rate <= 10; ++rate){
        double principal = 1000.00;

        std::cout << std::setw(15) << "Rate. " << rate << "%" << std::endl;
        std::cout << "\nYear" << std::setw(21) << "Amount on deposit" << std::endl;
        std::cout << std::fixed << std::setprecision(2);

        double percentage = 1.0 + rate * 0.01;
        for (int year = 1; year <= 10; ++year){
            double amount = principal * pow(percentage, year);

            std::cout << std::setw(4) << year << std::setw(21) << amount << std::endl;
        }
        std::cout << std::endl;
    }

    return 0;
}
