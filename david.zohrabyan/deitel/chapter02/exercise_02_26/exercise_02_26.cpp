/// This program prints a chess pattern
#include <iostream> /// standard input output

/// function main begins program execution
int
main()
{
    /// output a chess pattern
    std::cout << " * * * * * * * *  \n";
    std::cout << "  * * * * * * * * \n";
    std::cout << " * * * * * * * *  \n";
    std::cout << "  * * * * * * * * \n";
    std::cout << " * * * * * * * *  \n";
    std::cout << "  * * * * * * * * \n";
    std::cout << " * * * * * * * *  \n";
    std::cout << "  * * * * * * * * \n" << std::endl;

  

    /// output a chess pattern
    std::cout << " * * * * * * * * \n  * * * * * * * * \n * * * * * * * * \n  * * * * * * * * \n * * * * * * * * \n  * * * * * * * * \n * * * * * * * * \n  * * * * * * * * \n";

    return 0; /// program ended successufully
} /// end function main

  
