#include <iostream>

int
main()
{
    std::cout << "This program defines also prints, whether they can represent the parties of a rectangular triangle." << std::endl;   
    
    int a, b, c;  
    std::cout << "Enter first length of a rectangular triangle (a > 0): " ;
    std::cin >> a;
    if(a <= 0) {
        std::cout << "Error 1. The length must be positive." << std::endl;
        return 1;
    }
    
    std::cout << "Enter second length of a rectangular triangle (b > 0): " ;
    std::cin >> b;
    if(b <= 0) {
        std::cout << "Error 1. The length must be positive." << std::endl;
        return 1;
    }    
    
    std::cout << "Enter third length of a rectangular triangle (c > 0): " ;
    std::cin >> c;
    if(c <= 0) {
        std::cout << "Error 1. The length must be positive." << std::endl;
        return 1;
    }

    a *= a;
    b *= b;
    c *= c;

    if(a == b + c ) {
        std::cout << "These parties can represent a rectangular triangle." << std::endl; 
    } else if(b == a + c) {
        std::cout << "These parties can represent a rectangular triangle." << std::endl;
    } else if(c == a + b) {
        std::cout << "These parties can represent a rectangular triangle." << std::endl;
    } else {
        std::cout << "These parties can`t represent a rectangular triangle." << std::endl;
    }
        
    return 0;
}
