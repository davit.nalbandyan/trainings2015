#include <iostream> 
#include <iomanip> 
#include <cmath>  
 
int
main() 
{
    double principal = 1000; 
    for (int rate = 5; rate <= 10; ++rate) {
        std::cout << "For " << rate << "% rate" << std::endl;
        std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::endl; 
        std::cout << std::fixed << std::setprecision(2); 

        double percent = 1 + rate / 100.00;
        for (int year = 1; year <= 10; ++year) {
            double amount = principal * std::pow(percent, year); 
            std::cout << std::setw(4)  << year << std::setw(21) << amount << std::endl; 
        } 
    
        std::cout << std::endl;
    }

    return 0; 
}  

