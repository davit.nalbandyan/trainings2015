#include <iostream>

int
main()
{
    int product = 1;
    for(int oddNumber = 1; oddNumber <= 15; oddNumber += 2) {
        product *= oddNumber;
    }
    std::cout << "Result of odd numbers 1 to 15 is " << product << std::endl;
   
    return 0;
}
