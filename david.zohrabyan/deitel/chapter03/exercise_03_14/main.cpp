/// Employee class testing
#include <iostream>
#include "Employee.hpp" /// Implementation of design’s Employee

int
main()
{
    Employee worker1("Brian", "Smith", 150); /// creates the first object of a class Employee and gives parameter
    Employee worker2("Cris", "Brown", 200); /// creates the second object of a class Employee and gives parameter
    Employee worker3("Mike", "Adams", 350); /// creates the third object of a class Employee and gives parameter
    
    std::cout << "List of workers." << std::endl;
    std::cout << "\tName" << "\tSurName" << "\tSalary" << std::endl;
    std::cout << "\t" << worker1.getName() << "\t" << worker1.getSurName() << "\t" << worker1.getSalary() << "\n";
    std::cout << "\t" << worker2.getName() << "\t" << worker2.getSurName() << "\t" << worker2.getSalary() << "\n";
    std::cout << "\t" << worker3.getName() << "\t" << worker3.getSurName() << "\t" << worker3.getSalary() << "\n" << std::endl;
    
    std::cout << "Yearly salary " << worker1.getName() << " is " << worker1.getAnnualWage() << std::endl;
    std::cout << "Yearly salary " << worker2.getName() << " is " << worker2.getAnnualWage() << std::endl;
    std::cout << "Yearly salary " << worker3.getName() << " is " << worker3.getAnnualWage() << std::endl;
    
    worker1.changeSalaryByPercent(10);
    worker2.changeSalaryByPercent(-1000);
    worker3.changeSalaryByPercent(10);
      
    std::cout << "Yealry salary after percent " << worker1.getName() << " is " << worker1.getAnnualWage() << std::endl;
    std::cout << "Yealry salary after percent " << worker2.getName() << " is " << worker2.getAnnualWage() << std::endl;
    std::cout << "Yealry salary after percent " << worker3.getName() << " is " << worker3.getAnnualWage() << std::endl;
    
    return 0;
}
